import unittest
import main

class TestStringMethods(unittest.TestCase):

    def test5Numbers(self):
        self.assertEqual(main.multiplyAllArrayElementsButI([1, 2, 3, 4, 5]), [120, 60, 40, 30, 24])

    def test2Numbers(self):
        self.assertEqual(main.multiplyAllArrayElementsButI([3, 2, 1]), [2, 3, 6])

    def testWithNegatives(self):
        self.assertEqual(main.multiplyAllArrayElementsButI([-2, 4, 2, -1]), [-8, 4, 8, -16])

if __name__ == '__main__':
    unittest.main()