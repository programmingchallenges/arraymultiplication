import sys

def main(numberList):
   print(multiplyAllArrayElementsButI(numberList)) 
   

def multiplyAllArrayElementsButI(numberList):
    result = []
    allNumbersMultiplied = 1
    for number in numberList:
        allNumbersMultiplied *= number

    for number in numberList:
        result.append(allNumbersMultiplied / number)

    return result

if __name__ == '__main__':
    main(sys.argv[1])